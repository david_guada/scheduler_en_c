#include <stdio.h>

#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_TAREAS 4

 
// Function to find the waiting time for all   
// processes  
void findWaitingTime(int processes[], int n,   
                          int bt[], int wt[])  
{  
    // waiting time for first process is 0  
    wt[0] = 0;  
    
    // calculating waiting time  
        for (int  i = 1; i < n ; i++ )  
        wt[i] =  bt[i-1] + wt[i-1] ;  
}  
    
// Function to calculate turn around time  
void findTurnAroundTime( int processes[], int n,   
                  int bt[], int wt[], int tat[])  
{  
    // calculating turnaround time by adding  
    // bt[i] + wt[i]  
    for (int  i = 0; i < n ; i++)  
        tat[i] = bt[i] + wt[i];  
}  
    
//Function to calculate average time  
void findavgTime( int processes[], int n, int bt[])  
{  
    int wt[n], tat[n], total_wt = 0, total_tat = 0;  
    
    
    //Function to find waiting time of all processes  
    findWaitingTime(processes, n, bt, wt);  
    
    //Function to find turn around time for all processes  
    findTurnAroundTime(processes, n, bt, wt, tat);  
    
    //tareas



    
    //Display processes along with all details  
    printf("Proceso   Tiempo de inicio   Tiempo de Espera   Tiempo de Respuesta\n");  
    
    // Calculando el tiempo de espera y de respuesta  
    for (int  i=0; i<n; i++)  
    {  
        total_wt = total_wt + wt[i];  
        total_tat = total_tat + tat[i]; 
        printf("   %d ",(i+1)); 
        printf("       %d\t\t ", bt[i] ); 
        printf("       %d\t\t",wt[i] ); 
        printf("       %d\n",tat[i] );
 
       
    }  
    	//for (int  i=0; i++) {
    	//int i;
    	//i= 0;
    	//i= i++;
    	
    	//int i++;
    	//total_wt = total_wt + wt[i];  
        //total_tat = total_tat + tat[i]; 
        printf("La tarea [%d], esta Escuchando Musica con un tiempo de respuesta de [%d]\n",/*(i+1),tat[i])*/(1), 10);
        printf("La tarea [%d], esta Leyendo un PDF con un tiempo de respuesta de [%d]\n", /*(i+1),tat[i])*/(2), 15);
        printf("La tarea [%d], esta Descargando un Archivo con un tiempo de respuesta de [%d]\n",/*(i+1),tat[i])*/ (3), 23);
        
		//}	
		//for(int j = 0; j < _array[i].delay; j++){
		    //process id's  
 
 
    
    int s=(float)total_wt / (float)n; 
    int t=(float)total_tat / (float)n; 
    printf("\nTiempo de espera promedio = %d",s); 
    printf("\n"); 
    printf("Tiempo de respuesta promedio = %d\n\n ",t);  
    
    
    printf(":::EJECUCION DE LAS TAREAS A TRAVES DE POSIX THREADS:::\n\n ",t);  
}  
      
      
	/*
	struct tarea{
	int processes;
	int tat;
	char nombre;
	char procesoarealizar;
	
};
	tarea *creartarea(int processes, int tat, char *nombre, char *procesoarealizar);
    	tarea *t1 = creartarea(1, tat, "Compilar Programa");
    	tarea *t2 = creartarea(2, tat, "Jugar videojuegos");
    	tarea *t3 = creartarea(3, tat, "Leer un PDF");
    	
    for (int  i=0; i<n; i++)  
    {  
        total_wt = total_wt + wt[i];  
        total_tat = total_tat + tat[i]; 
        printf("   %d ",(i+1)); 
        printf("       %d\t\t ", bt[i] ); 
        printf("       %d\t",wt[i] ); 
        printf("       %d\n",tat[i] );  
        printf("La tarea ")
    }  */
	
    
// Driver code  



  void *hacer_trabajo(void *argumentos){
  int index = *((int *)argumentos);
  int sleep_time = 3 + rand() % NUM_TAREAS;
  printf("Tarea %d: Inicio.\n", index);
  printf("Tarea %d: Suspendido por %d segundos.\n", index, sleep_time);
  sleep(sleep_time);
  printf("Tarea %d: Finalizado.\n", index);
  
}


int main(void)  
{  
    //process id's  
    int processes[] = {1, 2, 3};  
    int n = sizeof processes / sizeof processes[0];  
    

    //Burst time of all processes  
    int  burst_time[] = {10, 5, 8};  
    
    findavgTime(processes, n,  burst_time);  
    
	
	

  pthread_t tareas[NUM_TAREAS];
  int tareas_args[NUM_TAREAS];
  int j;
  int result_code;
  
  //Creando hilo por hilo, solo creacion
  for (j = 1; j < NUM_TAREAS; j++) {
    printf("\nEstado principal: Creando Tarea %d.\n", j);
    tareas_args[j] = j;
    result_code = pthread_create(&tareas[j], NULL, hacer_trabajo, &tareas_args[j]);
    assert(!result_code);
  }

  //printf("Estado principal: Hilos de las tareas creados.\n");

  //Espera del termino de hilo
  for (j = 1; j < NUM_TAREAS; j++) {
    result_code = pthread_join(tareas[j], NULL);
    assert(!result_code);
    printf("Estado principal: Tarea %d ha finalizado.\n", j);
  }

  printf("\nTodos las tareas han finalizado.\n");
  return 0;
		  
}  


// POSIX THREADS 


